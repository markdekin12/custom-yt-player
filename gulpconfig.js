/******************
 * CONFIGURE HERE *
 ******************/

/******* For use with "gulp watch" *******/

// First, enter whether or not gulp watch is used in the default "gulp".
var watchInDefault = false;
// Second, enter the URI to which the browser will open to.
var watchURI = 'http://customytplayer.dev/';
// Third, select which of the below groups of files to watch
var watchStyles = false;
var watchScripts = true;
var watchImages = false;
var watchUploads = false;

/******* For use with "gulp styles" *******/

// First, enter whether or not gulp styles is used in the default "gulp".
var stylesInDefault = false;
// Second, enter the Source Directory which contains the styles files. This is relative to where the gulpfile.js file exists.
var stylesSrcDir = './assets/styles/';
// Third, enter source style file name. It is not an array to be concatenated because of the @import rule used in Sass/CSS.
var stylesSrcFile = 'main.scss';
// Fifth, enter the Distributable Directory that would be created for the concatenated styles file. This is relative to where the gulpfile.js file exists.
var stylesDistDir = './dist/styles/';
// Sixth, enter the file name you would like to give to the styles distributable file. The style's gulping will create a concatenated version with this filename.css and it will also create a minified version with the min prefix: filename.min.css
var stylesDistFilename = 'main';

/******* For use with "gulp scripts" *******/

// First, enter whether or not gulp scripts is used in the default "gulp".
var scriptsInDefault = true;
// Second, enter the Source Directory which contains the script files. This is relative to where the gulpfile.js file exists.
var scriptsSrcDir = './assets/scripts/';
// Third, enter the order in which these source script files should be concatenated. Remember to include the remaining URI if any files are nested deeper that the source URI defined above.
var scriptsConcatOrder = [
  'main.js',
  'getPlayerVars.js',
  'initHTML.js',
  'updatePoster.js',
  'setVolume.js',
  'updateProgress.js',
  'trackProgress.js',
  'stopProgress.js',
  'setProgress.js',
  'requestPlayerFullScreen.js',
  'hookUpControls.js',
  'generateList.js',
  'playerReady.js',
  'secondsToTime.js',
  'playerStateChange.js',
  'init.js',
  // 'example.js',
];
// Fourth, enter the scripts plugin glob. This exists so that it may ignore jshint, preventing uneccessary errors from external script plugins. It doesn't matter if none exists.
var scriptsPluginsGlob = './assets/scripts/plugins/external/**/*.js';
// Fifth, enter the Distributable Directory that would be created for the concatenated scripts file. This is relative to where the gulpfile.js file exists.
var scriptsDistDir = './dist/scripts/';
// Sixth, enter the file name you would like to give to the scripts distributable file. The script's gulping will create a concatenated version with this filename.js and it will also create a minified version with the min prefix: filename.min.js
var scriptsDistFilename = 'custom-yt-player';

/******* For use with "gulp images" *******/
// First, enter whether or not gulp images is used in the default "gulp".
var imagesInDefault = false;
// Second, enter the Source Directory which contains the image files. This is relative to where the gulpfile.js file exists.
var imagesSrcDir = './assets/images/';
// Third, enter the Distributable Directory that would be created for the optimized images. This is relative to where the gulpfile.js file exists.
var imagesDistDir = './dist/scripts/';

/******* For use with "gulp uploads" - WordPress only *******/
// First, enter whether or not gulp uploads is used in the default "gulp".
var uploadsInDefault = false;
// Second, enter the Source Directory which contains the image files. This is relative to where the gulpfile.js file exists.
var unploadsSrcGlob = '../../uploads/**/*';


/*****************************************
 * CONFIGURE END - NO NEED TO EDIT BELOW *
 *****************************************/

function concatWithSrcDir(concat, srcDir) {
  for (var i = 0; i < concat.length; i++) {
    concat[i] = srcDir+concat[i];
  }
  return concat;
}

var defaultsArray = [];
var preWatchArray = [];

if (stylesInDefault) {
  defaultsArray.push('styles');
}

if (scriptsInDefault) {
  defaultsArray.push('scripts');
}

if (imagesInDefault) {
  defaultsArray.push('images');
}

if (uploadsInDefault) {
  defaultsArray.push('uploads');
}

if (watchInDefault) {
  defaultsArray.push('watch');
}

var scriptsConcat = concatWithSrcDir(scriptsConcatOrder, scriptsSrcDir);

var gulpconfig = {
  defaultsArray: defaultsArray,
  scripts: {
    srcDir: scriptsSrcDir,
    concat: scriptsConcat,
    pluginsGlob: scriptsPluginsGlob,
    distDir: scriptsDistDir,
    distFilename: scriptsDistFilename,
  },
  styles: {
    srcDir: stylesSrcDir,
    srcFile: stylesSrcFile,
    distDir: stylesDistDir,
    distFilename: stylesDistFilename,
  },
  images: {
    srcDir: imagesSrcDir,
    distDir: imagesDistDir,
  },
  uploads: {
    srcGlob: unploadsSrcGlob,
  },
  watch: {
    proxy: watchURI,
    styles: watchStyles,
    scripts: watchScripts,
    images: watchImages,
    uploads: watchUploads,
  },
};

module.exports = gulpconfig;