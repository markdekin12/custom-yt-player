CustomYTPlayer.prototype.setProgress = function(event, pos, progressContainer, buffer){
  var obj = this;
  var options = obj.options;
  var newPercent;
  var newTime;

  if (options.controls.progressAxis === 'horizontal') {
    newPercent = Math.max( 0, Math.min(1, (pos - progressContainer.offset().left) / progressContainer.width()) );
  } else if (options.controls.progressAxis === 'vertical') {
    var bottomOffset = progressContainer.offset().top+progressContainer.height();
    newPercent = Math.max( 0, Math.min(1, ((pos-bottomOffset)*(-1)/progressContainer.height())));
  }

  newTime = Math.round(newPercent*event.target.getDuration());
  event.target.seekTo(newTime, buffer);
  obj.selectors.controls.find('.cyt-current-time-amount').text(obj.secondsToTime(newTime));

  if (options.controls.progressAxis === 'horizontal') {
    progressContainer.find('.cyt-progress-bar').css('width', (Math.round((newPercent)*10000)/100)+'%');
  } else if (options.controls.progressAxis === 'vertical') {
    progressContainer.find('.cyt-progress-bar').css('height', (Math.round((newPercent)*10000)/100)+'%');
  }
};