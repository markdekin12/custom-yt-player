CustomYTPlayer.prototype.playerReady = function(event) {
  var obj = this;
  var options = obj.options;
  obj.selectors.player = $(event.target.getIframe());

  if (!obj.isPlaylist && !options.playlist.autoplayOnLoad) {

    event.target.cueVideoById({
      videoId: options.playlist.videoId
    });

  } else if (!obj.isPlaylist && options.playlist.autoplayOnLoad) {

    if (obj.isCustomControls) {
      obj.states.closingPoster = true;
    }

    event.target.loadVideoById({
      videoId: options.playlist.videoId
    });

  } else if (obj.isPlaylist && !options.playlist.autoplayOnLoad) {

    if (typeof options.playlist.list === 'string') {
      event.target.cuePlaylist({
        listType: options.playlist.type,
        list: options.playlist.list,
        index: options.playlist.index
      });
    } else {
      event.target.cuePlaylist(options.playlist.list);
    }
    

  } else if (obj.isPlaylist && options.playlist.autoplayOnLoad) {

    if (obj.isCustomStartButton) {
      obj.states.closingPoster = true;
    }

    if (typeof options.playlist.list === 'string') {
      event.target.loadPlaylist({
        listType: options.playlist.type,
        list: options.playlist.list,
        index: options.playlist.index
      });
    } else {
      event.target.loadPlaylist(options.playlist.list);
    }

  }

  if (obj.isCustomControls && (options.controls.location === 'external' || options.controls.location === 'before' || options.controls.location === 'after')) {
    var volume = options.controls.volumeStart;
    var playbackRate = event.target.getPlaybackRate();
    obj.selectors.controls = $('.'+obj.playerId+'-controls');
    obj.hookUpControls(event);
    event.target.setVolume(volume);
    obj.selectors.controls.find('.cyt-vol-amount').text(volume);

    if (options.controls.volumeAxis === 'vertical') {
      obj.selectors.controls.find('.cyt-vol-bar').css('height', volume+'%');
    } else if (options.controls.volumeAxis === 'horizontal') {
      obj.selectors.controls.find('.cyt-vol-bar').css('width', volume+'%');
    }
  }

  if (obj.isCustomStartButton) {

    obj.selectors.poster.click(function(){
      obj.states.closingPoster = true;
      event.target.seekTo(0.1);
      event.target.playVideo();
    });
  }

  if (options.playlist.loop) {
    event.target.setLoop(true);
  }
};