CustomYTPlayer.prototype.requestPlayerFullScreen = function(event) {
  var obj = this;
  var player = obj.selectors.playerWrapper[0];
  // Supports most browsers and their versions.

  var requestMethod = player.requestFullScreen || player.webkitRequestFullScreen || player.mozRequestFullScreen || player.msRequestFullscreen;

  if (requestMethod) { // Native full screen.
      requestMethod.call(player);
  } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");
    if (wscript !== null) {
      wscript.SendKeys("{F11}");
    }
  }
};