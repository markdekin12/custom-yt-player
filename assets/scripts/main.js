var CustomYTPlayer = function(playerId, customOptions) {
  this.playerId = playerId;
  this.defaultOptions = {
    classes: {
      player: '',
      playerWrapper: '',
      container: '',
      controls: '',
      playlistContainer: ''
    },
    playlist: {
      type: false,
      list: '',
      videoId: '',
      index: 0,
      loop: false,
      autoplayOnLoad: false,
      autoplayOnChange: true,
      autoplayOnEnd: true,
      showList: false,
      location: 'external',
      itemGenerate: '<li class="cyt-playlist-item"><div data-type="img" class="cyt-playlist-image"></div></li>',
    },
    display: {
      iframeHeight: 360,
      iframeWidth: 640,
      bareVideo: false,
      hideControls: false,
      showInfo: true,
      videoAnnotations: true,
      relatedVideos: true,
      fullscreenButton: true,
      color: 'red',
      modestBranding: false,
      forceCC: false,
      interfaceLanguage: false
    },
    customStartButton: {
      useDefault: true,
      type: 'none', // if useDefault is false
      src: '', // if type is 'src'
      html: '', // if type is 'html'
      width: '25%',
      height: '25%',
      position: 'center'
    },
    controls: {
      custom: false,
      allowKeyboardControls: true, // make custom keyboard control options an option
      location: 'external',
      html: '',
      progressAxis: 'horizontal',
      volumeStart: 50,
      volumeAxis: 'vertical',
    },
  };

  this.options = $.extend(true, this.defaultOptions, customOptions);

  this.selectors = {
    player: $('#'+this.playerId),
    playerWrapper: '',
    playlist: '',
    container: '',
    posterWrapper: '',
    poster: '',
    progressContainer: '',
    progressBar: '',
    controls: '',
  };

  this.states = {
    playlistGenerated: false,
    newVideoCued: false,
    cuedAfterEnd: false,
    showingPoster: false,
    closingPoster: false,
//     ccOn: false
  };

  this.temp = {
    videoId: '',
    progressInterval: false,
    currentVolume: 0,
  };

  this.playlistArray = [];

  if (this.options.playlist.type) {
    this.isPlaylist = true;
  } else {
    this.isPlaylist = false;
    this.temp.videoId = this.options.playlist.videoId;
  }

  if (!this.options.customStartButton.useDefault) {
    this.isCustomStartButton = true;
  } else {
    this.isCustomStartButton = false;
  }

  if (this.options.controls.custom) {
    this.isCustomControls = true;
  } else {
    this.isCustomControls = false;
  }
};