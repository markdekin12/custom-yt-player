CustomYTPlayer.prototype.setVolume = function(event, pos, volContainer){
  var obj = this;
  var options = obj.options;
  var newPercent;

  if (options.controls.volumeAxis === 'vertical') {
    var bottomOffset = volContainer.offset().top+volContainer.height();
    newPercent = Math.round(Math.max( 0, Math.min(1, ((pos-bottomOffset)*(-1)/volContainer.height())))*100);
  } else if (options.controls.volumeAxis === 'horizontal') {
    newPercent = Math.round(Math.max( 0, Math.min(1, (pos - volContainer.offset().left) / volContainer.width()))*100);
  }

  if (event.target.isMuted()) {
    event.target.unMute();
    obj.selectors.container.removeClass('muted');
    obj.selectors.controls.removeClass('muted');
  }

  event.target.setVolume(newPercent);

  obj.selectors.controls.find('.cyt-vol-amount').text(newPercent);

  if (options.controls.volumeAxis === 'vertical') {
    volContainer.find('.cyt-vol-bar').css('height', newPercent+'%');
  } else if (options.controls.volumeAxis === 'horizontal') {
    volContainer.find('.cyt-vol-bar').css('width', newPercent+'%');
  }
  
};