CustomYTPlayer.prototype.updateProgress = function(event, progressBar) {
  var obj = this;
  var options = obj.options;
  var newPercent = (Math.round((event.target.getCurrentTime()/event.target.getDuration())*10000)/100)+'%';

  obj.selectors.controls.find('.cyt-current-time-amount').text(obj.secondsToTime(Math.round(event.target.getCurrentTime())));

  var bufferAmount = Math.round(event.target.getVideoLoadedFraction()*10000)/100;
  obj.selectors.controls.find('.cyt-buffer-bar').css('width', bufferAmount+'%');
  obj.selectors.controls.find('.cyt-buffer-amount').text(Math.round(bufferAmount));
  if (options.controls.progressAxis === 'horizontal') {
    progressBar.css('width', newPercent);
  } else if (options.controls.progressAxis === 'vertical') {
    progressBar.css('height', newPercent);
  }
};