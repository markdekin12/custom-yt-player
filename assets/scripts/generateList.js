CustomYTPlayer.prototype.generateList = function(event) {
  var obj = this;
  var options = obj.options;
  var playlist = obj.selectors.playlist;
  
  obj.playlistArray = event.target.getPlaylist();

  for (var i = 0; i < obj.playlistArray.length; i++) {
    var id = obj.playlistArray[i];
    var preGenHtml = $(options.playlist.itemGenerate);
    var listImage = preGenHtml.find('.cyt-playlist-image');
    var imageType = listImage.attr('data-type');
    preGenHtml.filter('.cyt-playlist-item').attr('data-vidnum', i);

    if (i === options.playlist.index) {
      preGenHtml.filter('.cyt-playlist-item').addClass('current');
    }

    if (imageType === 'img') {
      listImage.html('<img alt="YouTube playlist image" src="https://img.youtube.com/vi/'+id+'/0.jpg">');
    } else if (imageType === 'background') {
      listImage.css('background-image', 'url(https://img.youtube.com/vi/'+id+'/0.jpg)');
    }

    playlist.append(preGenHtml);
  }

  playlist.on('click', '.cyt-playlist-item', function(){
    var clicked = $(this);
    if (!clicked.hasClass('current')) {
      var vidKey = clicked.data('vidnum');
      clicked.parents('.cyt-playlist').find('.cyt-playlist-item.current').removeClass('current');
      clicked.addClass('current');
      clicked.parents('.cyt-playlist').trigger('newItem');
      obj.states.newVideoCued = true;
      event.target.playVideoAt(vidKey);
    }
  });

  obj.states.playlistGenerated = true;
};