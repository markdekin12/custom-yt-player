CustomYTPlayer.prototype.getPlayerVars = function(){
  var obj = this;
  var options = obj.options;
  var playerVars = {};

  /* Missing playerVars:
  * autoplay, enableJsApi, widgetRefferer, listtype, list, playlist, playsinline, start, & end
  */

  if (options.display.bareVideo || options.display.hideControls) {
    playerVars.controls = 0;
  } else {

    if (!options.display.fullscreenButton) {
      playerVars.fs = 0;
    }

    if (options.display.color === 'white') {
      playerVars.color = 'white';
    } else {

      if (options.display.modestBranding) {
        playerVars.modestbranding = 1;
      }
    }
  }

  if (options.playlist.loop) {
    playerVars.loop = 1;
  }

  if (options.display.bareVideo || !options.display.showInfo) {
    playerVars.showinfo = 0;
  }

  if (options.display.bareVideo || !options.display.videoAnnotations) {
    playerVars.iv_load_policy = 3;
  }

  if (options.display.bareVideo || !options.display.relatedVideos) {
    playerVars.rel = 0;
  }

  if (options.display.forceCC) {
    playerVars.cc_load_policy = 1;
  }

  if (!options.controls.allowKeyboardControls) {
    playerVars.disablekb = 1;
  }

  if (typeof options.display.interfaceLanguage === 'string') {
    playerVars.h1 = options.display.interfaceLanguage;
  }

  return playerVars;
};