CustomYTPlayer.prototype.init = function() {
  var obj = this;
  var options = obj.options;
  var yt_int, yt_players={};
  var playerVars = obj.getPlayerVars();

  obj.initHTML();

  function onPlayerReady(event) {
    obj.playerReady(event);
  }

  function onPlayerStateChange(event) {
    obj.playerStateChange(event);
  }

  // function onApiChange(event) {
  //   console.log('onApiChange');
  //   // event.target.loadModule("captions");  
  //   // event.target.loadModule("cc");
  //   console.log(event.target.getOption('captions', 'displaySettings'));
  //   // if (event.target.getOptions() === 'captions') {
  //     event.target.setOption('captions', 'displaySetting', true);
  //   // }
    
  //   // obj.onApiChange(event);
  // }

  var initYT = function() {
    yt_players[obj.playerId] = new YT.Player(obj.playerId,
    {
      height: options.display.iframeHeight,
      width: options.display.iframeWidth,
      playerVars: playerVars,
      events: {
        'onReady' : onPlayerReady,
        'onStateChange': onPlayerStateChange,
        // 'onApiChange': onApiChange
      }
    });
  };

  $.getScript("//www.youtube.com/player_api", function() {
    yt_int = setInterval(function() {
      if (typeof YT === "object") {
        initYT();
        clearInterval(yt_int);
      }
    },300);
  });
};