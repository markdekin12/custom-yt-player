CustomYTPlayer.prototype.playerStateChange = function(event) {
  var obj = this;
  var options = obj.options;
  var state = event.data;
  console.log(state);

  if (state === -1) {
    obj.selectors.container.removeClass('unstarted ended playing paused buffering cued').addClass('unstarted');

    if (obj.isCustomControls) {
      obj.selectors.controls.removeClass('unstarted ended playing paused buffering cued').addClass('unstarted');
    }

    if (obj.isPlaylist && options.playlist.showList) {
      var playlistItems = obj.selectors.playlist.find('.cyt-playlist-item');
      var newPlaylistItem = playlistItems.filter('[data-vidnum="'+event.target.getPlaylistIndex()+'"]');
      var oldPlaylistItem = playlistItems.filter('.current');

      if (newPlaylistItem[0] !== oldPlaylistItem[0]) {
        oldPlaylistItem.removeClass('current');
        newPlaylistItem.addClass('current');
        newPlaylistItem.parents('.cyt-playlist').trigger('newItem');
      }
    }

    if (obj.isPlaylist && ((!options.playlist.autoplayOnChange && obj.states.newVideoCued) || (!options.playlist.autoplayOnEnd && obj.states.cuedAfterEnd))) {
      event.target.stopVideo();
      obj.states.newVideoCued = false;
      obj.states.cuedAfterEnd = false;
    } else if (obj.isPlaylist && (options.playlist.autoplayOnChange && obj.states.newVideoCued)) {
      obj.states.closingPoster = true;
    }

    if(obj.isPlaylist && options.playlist.showList && !obj.states.playlistGenerated) {
      obj.generateList(event);
    }

  } else if (state === 0) {
    obj.selectors.container.removeClass('unstarted ended playing paused buffering cued').addClass('ended');

    if (obj.isCustomControls) {
      obj.selectors.controls.removeClass('unstarted ended playing paused buffering cued').addClass('ended');
      obj.stopProgress(event);
    }

    if (obj.isPlaylist && !options.playlist.autoplayOnEnd) {
      obj.states.cuedAfterEnd = true;
    }

    if (!obj.isPlaylist && options.playlist.loop) {
      event.target.playVideo();
    } else if (!options.playlist.loop && (!obj.isPlaylist || (obj.isPlaylist && event.target.getPlaylistIndex() === obj.playlistArray.length-1))) {
      obj.updatePoster(event);
    }

  } else if (state === 1) {
    obj.selectors.container.removeClass('unstarted ended playing paused buffering cued').addClass('playing');

    if (obj.isCustomControls) {
      obj.selectors.controls.removeClass('unstarted ended playing paused buffering cued').addClass('playing');
      obj.trackProgress(event);
      var durationSeconds = Math.round(event.target.getDuration());
      var time = obj.secondsToTime(durationSeconds);
      var playbackRate = event.target.getPlaybackRate();
      $('.'+obj.playerId+'-controls .cyt-duration-amount').text(time);
      obj.selectors.controls.find('.cyt-playback-rate-amount').text(playbackRate);
    }

    if (obj.states.closingPoster) {
      obj.selectors.poster.fadeOut(300);
      obj.temp.thePoster = '';
      obj.states.showingPoster = false;
      obj.states.closingPoster = false;
    }

  } else if (state === 2) {
    obj.selectors.container.removeClass('unstarted ended playing paused buffering cued').addClass('paused');

    if (obj.isCustomControls) {
      obj.selectors.controls.removeClass('unstarted ended playing paused buffering cued').addClass('paused');
      obj.stopProgress(event);
    }

  } else if (state === 3) {
    obj.selectors.container.removeClass('unstarted ended playing paused buffering cued').addClass('buffering');

    if (obj.isCustomControls) {
      obj.selectors.controls.removeClass('unstarted ended playing paused buffering cued').addClass('buffering');
    }

    if (obj.states.closingPoster) {
      obj.selectors.poster.fadeOut(300);
      obj.temp.thePoster = '';
      obj.states.showingPoster = false;
      obj.states.closingPoster = false;
    }
  } else if (state === 5) {
    obj.selectors.container.removeClass('unstarted ended playing paused buffering cued').addClass('cued');

    if (obj.isCustomControls) {
      obj.selectors.controls.removeClass('unstarted ended playing paused buffering cued').addClass('cued');
    }

    if (obj.isCustomStartButton) {
      obj.updatePoster(event);
    }
  }
};