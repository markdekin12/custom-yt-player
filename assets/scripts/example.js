var simpleYT = new CustomYTPlayer('tester', {
  playlist: {
    type: 'playlist',
    // list: 'PLOU2XLYxmsII8REpkzsy1bJHj6G1WEVA1',
    list: ['c9PF-9MIkiM', 'VOEl3ERptCs'],
    // videoId: 'VA3j5_vKQfc',
    index: 0,
    loop: false,
    showList: true,
    location: 'after',
    autoplayOnLoad: false,
    autoplayOnChange: false,
    autoplayOnEnd: true,
  },
  display: {
    bareVideo: true,
  },
  customStartButton: {
    useDefault: false,
    type: 'html',
    html: '<i class="fa fa-play-circle"></i>',
    width: '16%',
  },
  controls: {
    custom: true,
    location: 'after',
    html: '<div class="cyt-progress-container background-light-grey" style="width:640px; height:1rem;position:relative"><div class="cyt-buffer-bar background-grey" style="height:100%;"></div><div class="cyt-progress-bar background-blue" style="height:100%; width:0%;position:absolute; bottom:0;"></div></div><button class="cyt-next"><span>Next</span></button><button type="button" class="cyt-fullscreen"><span>FullScreen</span></button>',
  }
});

simpleYT.init();
