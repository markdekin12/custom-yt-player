CustomYTPlayer.prototype.secondsToTime = function(inputSeconds) {
  var durationMinutes = Math.floor(inputSeconds/60);
  var seconds = inputSeconds%60;
  var hours = Math.floor(durationMinutes/60);
  var minutes = durationMinutes%60;
  var duration = hours +':'+ ('0'+minutes).slice(-2) +':'+ ('0'+seconds).slice(-2);
  return duration;
};