CustomYTPlayer.prototype.updatePoster = function(event) {
  var obj = this;
  var options = obj.options;  
  var thePlayer = obj.selectors.player;
  var thePoster = obj.selectors.poster;

  if (obj.isPlaylist) {
    obj.temp.playlist = event.target.getPlaylist();
    obj.temp.index = event.target.getPlaylistIndex();
    obj.temp.videoId = obj.temp.playlist[obj.temp.index];
  }

  if (!obj.states.showingPoster) {
    thePoster.show();
    obj.states.showingPoster = true;
  }
  thePoster.css('background-image', 'url(\'https://img.youtube.com/vi/'+obj.temp.videoId+'/0.jpg\')');
  // obj.selectors.poster = $('#'+obj.playerId+'-container .cyt-poster');
};