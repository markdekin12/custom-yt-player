CustomYTPlayer.prototype.trackProgress = function(event){
  var obj = this;
  var options = obj.options;
  var progressBar = $('.'+obj.playerId+'-controls .cyt-progress-bar');
  (function progressTrack() {
    obj.updateProgress(event, progressBar);
    obj.temp.progressInterval = setTimeout(progressTrack, 50);
  })();
};