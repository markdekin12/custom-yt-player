CustomYTPlayer.prototype.hookUpControls = function(event) {
  var obj = this;
  var options = obj.options;
  var controls = obj.selectors.controls;
  var playerState = event.target.getPlayerState();
  var currentTime;
  var currentVolume;
  var duration;
  var progressMouse = false;

  controls.on('click', '.cyt-play', function(){
    event.target.playVideo();

    if (obj.states.showingPoster) {
      obj.states.closingPoster = true;
    }
  });

  controls.on('click', '.cyt-pause', function(){
    event.target.pauseVideo();
  });

  controls.on('click', '.cyt-play-pause', function() {
    var playerState = event.target.getPlayerState();

    if (playerState === 1) {
      event.target.pauseVideo();
    } else {
      event.target.playVideo();

      if (obj.states.showingPoster) {
        obj.states.closingPoster = true;
      }
    }
  });

  controls.on('click', '.cyt-prev', function(){
    // check if autoplay (if so then do obj.states.closingposter = true)
    obj.states.newVideoCued = true;
    event.target.previousVideo();
  });

  controls.on('click', '.cyt-next', function(){
    // check if autoplay (if so then do obj.states.closingposter = true)
    obj.states.newVideoCued = true;
    event.target.nextVideo();
  });

  controls.on('click', '.cyt-vol-up-x', function(){
    var clicked = $(this);
    currentVolume = event.target.getVolume();
    var percentUp = parseInt(clicked.data('percent'));
    var newVol = currentVolume;

    if (event.target.isMuted()) {
      event.target.unMute();
      obj.selectors.container.removeClass('muted');
      obj.selectors.controls.removeClass('muted');
    }

    if (currentVolume !== 100 && currentVolume+percentUp >= 100) {
      newVol = 100;
    } else if (currentVolume+percentUp < 100) {
      newVol = currentVolume+percentUp;
    }

    event.target.setVolume(newVol);
    obj.selectors.controls.find('.cyt-vol-amount').text(newVol);
    if (options.controls.volumeAxis === 'vertical') {
      controls.find('.cyt-vol-bar').css('height', newVol+'%');
    } else if (options.controls.volumeAxis === 'horizontal') {
      controls.find('.cyt-vol-bar').css('width', newVol+'%');
    }
  });

  controls.on('click', '.cyt-vol-down-x', function(){
    var clicked = $(this);
    currentVolume = event.target.getVolume();
    var percentDown = parseInt(clicked.data('percent'));
    var newVol = currentVolume;

    if (event.target.isMuted()) {
      event.target.unMute();
      obj.selectors.container.removeClass('muted');
      obj.selectors.controls.removeClass('muted');
    }

    if (currentVolume !== 0 && currentVolume-percentDown <= 0) {
      newVol = 0;
    } else if (currentVolume-percentDown > 0) {
      newVol = currentVolume-percentDown;
    }

    event.target.setVolume(newVol);
    obj.selectors.controls.find('.cyt-vol-amount').text(newVol);
    if (options.controls.volumeAxis === 'vertical') {
      controls.find('.cyt-vol-bar').css('height', newVol+'%');
    } else if (options.controls.volumeAxis === 'horizontal') {
      controls.find('.cyt-vol-bar').css('width', newVol+'%');
    }
  });

  controls.on('click', '.cyt-mute', function(){

    if (!event.target.isMuted()) {
      event.target.mute();
      obj.selectors.container.addClass('muted');
      obj.selectors.controls.addClass('muted');
    }
  });

  controls.on('click', '.cyt-unmute', function(){

    if (event.target.isMuted()) {
      event.target.unMute();
      obj.selectors.container.removeClass('muted');
      obj.selectors.controls.removeClass('muted');
    }
  });

  controls.on('mousedown', '.cyt-vol-container', function(){
    var clicked = $(this);
    document.onmousemove = function(e){

      if (options.controls.volumeAxis === 'horizontal') {
        obj.setVolume(event, e.pageX, clicked);
      } else if (options.controls.volumeAxis === 'vertical') {
        obj.setVolume(event, e.pageY, clicked);
      }

      document.onmouseup = function(e) {
        document.onmouseup = null;
        document.onmousemove = null;

        if (options.controls.volumeAxis === 'horizontal') {
          obj.setVolume(event, e.pageX, clicked);
        } else if (options.controls.volumeAxis === 'vertical') {
          obj.setVolume(event, e.pageY, clicked);
        }
      };
    };

  });

  controls.on('click', '.cyt-mute-toggle', function(){
    if (event.target.isMuted()) {
      event.target.unMute();
      obj.selectors.container.removeClass('muted');
      obj.selectors.controls.removeClass('muted');
    } else {
      event.target.mute();
      obj.selectors.container.addClass('muted');
      obj.selectors.controls.addClass('muted');
    }
  });

  // controls.on('click', '.cyt-fullscreen', function(){
  //   obj.requestPlayerFullScreen(event);
  // });

  controls.on('click', '.cyt-back-x', function(){
    var clicked = $(this);
    currentTime = event.target.getCurrentTime();
    var backAmount = parseInt(clicked.data('seconds'));

    obj.stopProgress();

    if (obj.states.showingPoster) {
      obj.states.closingPoster = true;
    }

    if (currentTime-backAmount > 0) {
      event.target.seekTo(currentTime-backAmount);
    } else {
      event.target.seekTo(0.1);
    }
  });

  controls.on('click', '.cyt-forward-x', function(){
    var clicked = $(this);
    duration = event.target.getDuration();
    currentTime = event.target.getCurrentTime();
    var forwardAmount = parseInt(clicked.data('seconds'));

    obj.stopProgress();

    if (obj.states.showingPoster) {
      obj.states.closingPoster = true;
    }

    if ((duration-currentTime)-forwardAmount > 0) {
      event.target.seekTo(currentTime+forwardAmount, true);
    } else {
      event.target.seekTo(duration, true);
    }
  });

  controls.on('mousedown', '.cyt-progress-container', function(){
    var clicked = $(this);
    event.target.pauseVideo();

    document.onmousemove = function(e){
      if (options.controls.progressAxis === 'horizontal') {
        obj.setProgress(event, e.pageX, clicked, false);
      } else if (options.controls.progressAxis === 'vertical') {
        obj.setProgress(event, e.pageY, clicked, false);
      }
    };

    $(this).on('mouseup', function(e){
      document.onmouseup = null;
      document.onmousemove = null;
      if (options.controls.progressAxis === 'horizontal') {
        obj.setProgress(event, e.pageX, clicked, true);
      } else if (options.controls.progressAxis === 'vertical') {
        obj.setProgress(event, e.pageY, clicked, true);
      }
      event.target.playVideo();

      if (obj.states.showingPoster) {
        obj.states.closingPoster = true;
      }
    });
  });

  controls.on('click', '.cyt-playback-rate', function(){
    var rate = parseFloat($(this).data('rate'));
    event.target.setPlaybackRate(rate);

    controls.find('.cyt-playback-rate-amount').text(rate);
  });

  // controls.on('click', '.cyt-cc-toggle', function(){
  //   event.target.loadModule('captions');
  //   console.log('should be working');
  // });
};