CustomYTPlayer.prototype.initHTML = function() {
  var obj = this;
  var options = obj.options;
  var containerClass = 'cyt-container';

  obj.selectors.player.addClass('cyt-player');

  if (options.classes.player.trim() !== '' && typeof options.classes.player === 'string') {
    obj.selectors.player.addClass(options.classes.player);
  }

  if (typeof options.classes.container === 'string') {
    containerClass += ' '+options.classes.container;
  }

  obj.selectors.player.wrap('<div id="'+obj.playerId+'-container" class="'+containerClass+'"></div>');
  obj.selectors.container = $('#'+obj.playerId+'-container');

  if (obj.isPlaylist || obj.isCustomStartButton || obj.isCustomControls) {
    var playerWrapperClass = 'cyt-player-wrapper';

    if (typeof options.classes.playerWrapper === 'string') {
      playerWrapperClass += ' '+options.classes.playerWrapper;
    }

    obj.selectors.player.css('display', 'block');
    obj.selectors.player.wrap('<div class="'+playerWrapperClass+'" style="display:inline-block;"></div>');
    obj.selectors.playerWrapper = obj.selectors.player.parents('.cyt-player-wrapper');

    if (obj.isCustomStartButton) {
      obj.selectors.player.wrap('<div class="cyt-poster-wrapper" style="position: relative;"></div>');
      obj.selectors.posterWrapper = obj.selectors.container.find('.cyt-poster-wrapper');
      var posterHtml = '<div class="cyt-poster" style="background:#000 no-repeat center / cover;position:absolute;top:0;left:0;width:100%;height:100%;cursor:pointer;"></div>';
      obj.selectors.posterWrapper.append(posterHtml);
      obj.states.showingPoster = true;
      obj.selectors.poster = obj.selectors.posterWrapper.find('.cyt-poster');

      if (options.customStartButton.type !== 'none') {
        var buttonHtml;
        var buttonStyle = 'position:absolute;';
        var buttonPosition = options.customStartButton.position;

        buttonStyle += 'height:'+options.customStartButton.height+';';
        buttonStyle += 'width:'+options.customStartButton.width+';';

        if (buttonPosition === 'center') {
          buttonStyle += 'top:0;right:0;left:0;bottom:0;margin:auto';
        } else if (buttonPosition === 'left-top') {
          buttonStyle += 'top:15%;left:15%;';
        } else if (buttonPosition === 'left-center') {
          buttonStyle += 'top:0;left:10%;bottom:0;margin:auto';
        } else if (buttonPosition === 'left-bottom') {
          buttonStyle += 'left:10%;bottom:15%;';
        } else if (buttonPosition === 'center-top') {
          buttonStyle += 'top:15%;left:0;right:0;margin:auto';
        } else if (buttonPosition === 'center-bottom') {
          buttonStyle += 'left:0;right:0;bottom:15%;margin:auto';
        } else if (buttonPosition === 'right-top') {
          buttonStyle += 'top:15%;right:10%;';
        } else if (buttonPosition === 'right-center') {
          buttonStyle += 'top:0;right:10%;bottom:0;margin:auto';
        } else if (buttonPosition === 'right-bottom') {
          buttonStyle += 'right:10%;bottom:15%;margin:auto';
        }

        if (options.customStartButton.type === 'src') {
          buttonHtml = '<div class="custom-start-button" style="background: url(\''+options.customStartButton.src+'\') no-repeat center / contain;'+buttonStyle+'"></div>';
        } else if (options.customStartButton.type === 'html') {
          buttonHtml = '<button type="button" class="custom-start-button" style="'+buttonStyle+'">'+options.customStartButton.html+'</button>';
        }

        obj.selectors.poster.append(buttonHtml);
      }
    }

    if(obj.isCustomControls && (options.controls.location !== 'external')) {
      var controlsClass = obj.playerId+'-controls cyt-controls';

      if (options.classes.controls.trim() !== '' && typeof options.classes.controls === 'string') {
       controlsClass += ' '+options.classes.controls;
      }

      var controlPanelHtml = '<div class="'+controlsClass+'">'+options.controls.html+'</div>';
      if (options.controls.location === 'after') {
        obj.selectors.container.find('.cyt-player-wrapper').append(controlPanelHtml);
      } else if (options.controls.location === 'before') {
        obj.selectors.container.find('.cyt-player-wrapper').prepend(controlPanelHtml);
      }
    }

    if (obj.isPlaylist && options.playlist.showList) {

      if (options.playlist.location !== 'external') {
        var playlistClass = obj.playerId+'-playlist cyt-playlist';

        if (options.classes.playlistContainer.trim()!== '' && typeof options.classes.playlistContainer === 'string') {
          playlistClass += ' '+options.classes.playlistContainer;
        }

        var playlistContainerHtml = '<ul class="'+playlistClass+'"></ul>';
        if (options.playlist.location === 'before') {
          obj.selectors.container.prepend(playlistContainerHtml);
        } else if (options.playlist.location === 'after') {
          obj.selectors.container.append(playlistContainerHtml);
        }
      }

      obj.selectors.playlist = $('.'+obj.playerId+'-playlist');
    }
  }
};