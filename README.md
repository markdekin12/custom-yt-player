# Custom YT Player Documentation v.0.0.1
## Table of Contents
* [Initializing](#markdown-header-initializing)
* [Classes Options](#markdown-header-classes-options)
    * [classes.player](#markdown-header-classesplayer)
    * [classes.container](#markdown-header-classescontainer)
    * [classes.playerWrapper](#markdown-header-classesplayerwrapper)
    * [classes.controls](#markdown-header-classescontrols)
    * [classes.playlistContainer](#markdown-header-classesplaylistcontainer)
    * [Classes Example](#markdown-header-classes-example)
* [Playlist Options](#markdown-header-playlist-options)
    * [playlist.videoId](#markdown-header-playlistvideoid)
    * [playlist.type](#markdown-header-playlisttype)
    * [playlist.list](#markdown-header-playlistlist)
    * [playlist.index](#markdown-header-playlistindex)
    * [playlist.loop](#markdown-header-playlistloop)
    * [playlist.autoplayOnLoad](#markdown-header-playlistautoplayonload)
    * [playlist.autoplayOnChange](#markdown-header-playlistautoplayonchange)
    * [playlist.autoplayOnEnd](#markdown-header-playlistautoplayonend)
    * [playlist.showList](#markdown-header-playlistshowlist)
    * [playlist.location](#markdown-header-playlistlocation)
    * [playlist.itemGenerate](#markdown-header-playlistitemgenerate)
* [Display Options](#markdown-header-display-options)
    * [display.iframeHeight](#markdown-header-displayiframeheight)
    * [display.iframeWidth](#markdown-header-displayiframewidth)
    * [display.bareVideo](#markdown-header-displaybarevideo)
    * [display.hideControls](#markdown-header-displayhidecontrols)
    * [display.showInfo](#markdown-header-displayshowinfo)
    * [display.videoAnnotations](#markdown-header-displayvideoannotations)
    * [display.relatedVideos](#markdown-header-displayrelatedvideos)
    * [display.fullscreenButton](#markdown-header-displayfullscreenbutton)
    * [display.color](#markdown-header-displaycolor)
    * [display.modestBranding](#markdown-header-displaymodestbranding)
    * [display.forceCC](#markdown-header-displayforcecc)
    * [display.interfaceLanguage](#markdown-header-displayinterfacelanguage)
* [Custom Start Button Options](#markdown-header-custom-start-button-options)
    * [customStartButton.useDefault](#markdown-header-customstartbuttonusedefault)
    * [customStartButton.type](#markdown-header-customstartbuttontype)
    * [customStartButton.src](#markdown-header-customstartbuttonsrc)
    * [customStartButton.html](#markdown-header-customstartbuttonhtml)
    * [customStartButton.width](#markdown-header-customstartbuttonwidth)
    * [customStartButton.height](#markdown-header-customstartbuttonheight)
    * [customStartButton.position](#markdown-header-customstartbuttonposition)
* [Controls Options](#markdown-header-controls-options)
    * [controls.custom](#markdown-header-controlscustom)
    * [controls.allowKeyboardControls](#markdown-header-controlsallowkeyboardcontrols)
    * [controls.location](#markdown-header-controlslocation)
    * [controls.html](#markdown-header-controlshtml)
    * [controls.progressAxis](#markdown-header-controlsprogressaxis)
    * [controls.volumeStart](#markdown-header-controlsvolumestart)
    * [controls.volumeAxis](#markdown-header-controlsvolumeaxis)
* [Control Classes](#markdown-header-control-classes)
    * [.cyt-back-x](#markdown-header-cyt-back-x)
    * [.cyt-buffer-amount](#markdown-header-cyt-buffer-amount)
    * [.cyt-buffer-bar](#markdown-header-cyt-buffer-bar)
    * [.cyt-current-time-amount](#markdown-header-cyt-current-time-amount)
    * [.cyt-duration-amount](#markdown-header-cyt-duration-amount)
    * [.cyt-forward-x](#markdown-header-cyt-forward-x)
    * [.cyt-mute](#markdown-header-control-classes)
    * [.cyt-mute-toggle](#markdown-header-mute-toggle)
    * [.cyt-next](#markdown-header-cyt-next)
    * [.cyt-pause](#markdown-header-cyt-pause)
    * [.cyt-play](#markdown-header-cyt-play)
    * [.cyt-playback-rate](#markdown-header-cyt-playback-rate)
    * [.cyt-playback-rate-amount](#markdown-header-cyt-playback-rate-amount)
    * [.cyt-play-pause](#markdown-header-cyt-play-pause)
    * [.cyt-prev](#markdown-header-cyt-prev)
    * [.cyt-progress-container and .cyt-progress-bar](#markdown-header-cyt-progress-container-and-cyt-progress-bar)
    * [.cyt-unmute](#markdown-header-cyt-unmute)
    * [.cyt-vol-amount](#markdown-header-cyt-vol-amount)
    * [.cyt-vol-container and .cyt-vol-bar](#markdown-header-cyt-vol-container-and-cyt-vol-bar)
    * [.cyt-vol-down-x](#markdown-header-cyt-vol-down-x)
    * [.cyt-vol-up-x](#markdown-header-cyt-vol-up-x)

##Initializing
The following HTML and Javascript code is the most basic initializing of the *CustomYTPlayer*. In HTML, you will need an element with an id. In JavaScript you will pass this HTML id as the first parameter to your new instance of CustomYTPlayer(). The second parameter is an options object. In its simplest form, you will need to provide at least a YouTube video id inside a playlist object. Most of this documentation will focus on this second parameter and all of its various options.

Finally, to initialize your CustomYTPlayer you will need to pass the init() method to your instance.

The options object has 5 objects to which you can customize your instance of the *CustomYTPlayer*. Only the playlist onject is required.

* classes
* playlist
* display
* customStartButton
* controls

**NOTE: In order for all of this to work you need to be set up on a server, a localhost at the very least. It will not function correctly if this is done only in your local files.**

HTML

```
#!html

<div id="elementId"></div>
```

Javascript

```
#!javascript

var newYT = new CustomYTPlayer('elementId', {
  playlist: {
    videoId: 'M7lc1UVf-VE'
  }
});
newYT.init();
```

Once your instance of the *CustomYTPlayer()* is loaded into a browser the HTML element with your player id will be converted into an <iframe> and wrapped in at least one <div>. This container <div> will have your player id, the same id you passed as the first parameter in the prototype function, except with "-container" appended. This container will also have the "cyt-player" class. Whenever there is an update on the player's state, that container will also have a state class added or changed (i.e. cued, playing, paused, buffering, unstarted, and ended).

[Table of Contents](#markdown-header-table-of-contents)

##Classes Options
You may add additional classes to many of the HTML elements generated by the *CustomYTPlayer*. This is done by adding the classes object to the options object. Using the various classes options below to add your desired class(es) to specific generated hTML elements, see the [example below](#markdown-header-classes-example). Multiple classes may be added simply by separating each class by a space.

###classes.player
**player** (value: string)

Applies custom class(es) to the generated <iframe>.

[Table of Contents](#markdown-header-table-of-contents)

###classes.container
**container** (value: string)

Applies custom class(es) to the container that is always generated around the <iframe>. This is the outer most container element that is generated.

[Table of Contents](#markdown-header-table-of-contents)

###classes.playerWrapper
**playerWrapper** (value: string)

Applies custom class(es) to the element that is generated around the <iframe> but still within the outermost generated [container](#markdown-header-classescontainer). This element will be generated if there is a custom [playlist](#markdown-header-playlist-options) (only when not using the [videoId](#markdown-header-playlistvideoid) in the [playlist options](#markdown-header-playlist-options)), a custom start button, and/or there are custom controls (see [Controls Options](#markdown-header-controls-options)).

[Table of Contents](#markdown-header-table-of-contents)

###classes.controls
**controls** (value: string) (default: empty string)

Applied custom class(es) to any internally generated custom controls container. That is, if [controls.custom](#markdown-header-controlscustom) is set to true *and* [controls.location](#markdown-header-controlslocation) is either "after" or "before", then a controls wrapper would be internally generated and this class would be applied. Again, any external custom controls would not have this class applied to it, you would have to apply it manually.

[Table of Contents](#markdown-header-table-of-contents)

###classes.playlistContainer
**playlistContainer** (value: string) (default: empty string) (requires: [[playlist.type](#markdown-header-playlisttype): 'playlist' | 'user_uploads' | 'search'] && [[playlist.location](#markdown-header-playlistlocation): 'before' | 'after'])

Applies custom class(es) to a generated playlist container element. A playlist container is only generated if [playlist.type](#markdown-header-playlisttype) is not false, and [playlist.location](#markdown-header-playlistlocation) is either 'before' or 'after'.

[Table of Contents](#markdown-header-table-of-contents)

###Classes Example

```
#!javascript

var classesExampleYT = new CustomYTPlayer('exampleid', {
  playlist: {
    videoId: 'M7lc1UVf-VE'
  },
  classes: {
    player: 'example-player-class',
    container: 'example-container-class'
  }
});

```

[Table of Contents](#markdown-header-table-of-contents)

##Playlist Options
The *playlist* object is the one options object that is required for the *CustomYTPlayer* to function. Along with other playlist related options, this object will mainly be used to establish which video(s) the player will load. To do this, you determine which type of playlist it will be ([playlist.type](#markdown-header-playlisttype) it is false by default) which means that the player only loads a single video. As can be seen in the [Initializing](#markdown-header-initializing) section, the most basic option for the playlist object is [videoId](#markdown-header-playlistvideoid), assuming that [playlist.type](#markdown-header-playlisttype) remains false.

###playlist.videoId
**videoId** (value: string) (default: empty string) (requires: [playlist.type](#markdown-header-playlisttype): false)

The YouTube video id you'd wish to load. Only used if the [playlist type](#markdown-header-playlisttype) is set to false, meaning there is no "list" and the intention is to have a single video.

[Table of Contents](#markdown-header-table-of-contents)

###playlist.type
**type** (value: false (boolean) or string) (default: false (boolean))

**value options**

* *false* (boolean) (default) leaving the type parameter as false will mean that the player does not have a play*list* and will look to [playlist.videoId](#markdown-header-playlistvideoid) for the single video ID.
* *'playlist'* (string) A YouTube playlist and you must set [playlist.list](#markdown-header-playlistlist) to either a YouTube playlist ID or an array of YouTube video IDs.
* *'user_uploads'* (string) Uploads on a YouTube channel and you must set [playlist.list](#markdown-header-playlistlist) to the YouTube channel's ID.
* *'search'* (string) Search query, simulates doing a search on YouTube's search bar and the playlist retrieved would be the order of those results. You would then set the [playlist.list](#markdown-header-playlistlist) to the search query.

[Table of Contents](#markdown-header-table-of-contents)

###playlist.list
**list** (value: string | array) (default: empty string) (requires: [playlist.type](#markdown-header-playlisttype): 'playlist' | 'user_uploads' | 'search')

* a playlist ID or an array of video IDs (if [playlist.type](#markdown-header-playlisttype): 'playlist')
* channel ID (if [playlist.type](#markdown-header-playlisttype): 'user_uploads')
* a search query (if [playlist.type](#markdown-header-playlisttype): 'search')

[Table of Contents](#markdown-header-table-of-contents)

###playlist.index
**index** (value: integer) (default: 0 (integer)) (requires: [playlist.type](#markdown-header-playlisttype): 'playlist' | 'user_uploads' | 'search')

Which video from the expected returned playlist array that you wish the player to load first, when the player initially loads. The indexing begins at 0, not 1.

[Table of Contents](#markdown-header-table-of-contents)

###playlist.loop
**loop** (value: boolean) (default: false)

* *true* - if a single video, the video will play again once it has ended. If there is a list of videos, then once the last video in the playlist has ended it will play the first video in the list. Additionally, playlist looping applies to the any next and/or previous video controls.
* *falst* - No looping of videos or playlists.

###playlist.autoplayOnLoad
**autoplayOnLoad** (value: boolean) (default: false)

Whether or not the first video will play once the player is ready. It is recommended for this to remain false. This option does not determine autoplay if a new video is selected or if a subsequent video is cued, use the [playlist.autoplayOnChange](#markdown-header-playlistautoplayonchange) parameter instead.

[Table of Contents](#markdown-header-table-of-contents)

###playlist.autoplayOnChange
**autoplayOnChange** (value: boolean) (default: true)

Whether or not the player will autoplay once a new video is cued (e.g. by clicking next, or selecting a new video out of a list of videos). This option does not determine if the first video will play on the initial loading of the player, use the [playlist.autoplayOnLoad](#markdown-playlistautoplayonload) parameter instead. It also does not apply to video that are cued because the current video has reach the end of its playback time, for that option see [playlist.autoplayOnEnd](#markdown-header-playlistautoplayonend).

[Table of Contents](#markdown-header-table-of-contents)

###playlist.autoplayOnEnd
**autoplayOnEnd** (value: boolean) (default: true) (requires: [playlist.type](#markdown-header-playlisttype): 'playlist' | 'user_uploads' | 'search')

If set to false, will not automatically play the next video once it loads, when caused by the current video reaching the end of its playback time.

[Table of Contents](#markdown-header-table-of-contents)

###playlist.showList
**showList** (value: boolean) (default: false) (requires: [playlist.type](#markdown-header-playlisttype): 'playlist' | 'user_uploads' | 'search')

* *true* - Find and/or create a playlist element (see [playlist.location](#markdown-header-playlistlocation)) and within it, allow a list of videos to be generated (see [playlist.itemGenerate](#markdown-header-playlistitemgenerate)). 
* *false* A custom playlist will not show or be generated, however the player will still play through the playlist.

[Table of Contents](#markdown-header-table-of-contents)

###playlist.location
**location** (value: string) (default: 'external') (requires: [[playlist.type](#markdown-header-playlisttype): 'playlist' | 'user_uploads' | 'search'] && [[playlist.showList](#markdown-header-playlistshowlist): true])

**value options**

* *'external'* (string) (default) Playlist is outside of the player's outermost generated container element. You will need to make an HTML element somewhere on the page that has both the 'cyt-playlist' and 'playerid-playlist' classes, where 'playerid' is the the same as the HTML ID that was given to the main element.
* *'before'* (string) prepend the generated playlist container just inside of the outermost generated player container element.
* *'after'* (string) append the generated playlist container just inside of the outermost generated player container element.

[Table of Contents](#markdown-header-table-of-contents)

###playlist.itemGenerate
**itemGenerate** (value: HTML string) (default: *see below*) (requires: [[playlist.type](#markdown-header-playlisttype): 'playlist' | 'user_uploads' | 'search'] && [[playlist.showList](#markdown-header-playlistshowlist): true])

HTML that will be generated per video in a playlist. This HTML string can contain the following classes:

* *cyt-playlist-item* - This will indicate the element that will trigger the targeted video when clicked. This class should always be used, and usually works best as the outermost element of the HTML string.
* *cyt-playlist-image* - This class, if added, works along with a data attribute 'data-type' which works with either 'img' or 'background' as the data-type value. If 'img', the 'cyt-playlist-image' element will append an <img> inside. If, 'background', the image will be added as a 'background-image'  to the 'cyt-playlist-image' element.

The default HTML string is as follow:

'<li class="cyt-playlist-item"><div data-type="img" class="cyt-playlist-image"></div></li>'

**NOTE**: The default HTML string is a <li> tag and the default 'cyt-playlist' element, if [playlist.location](#markdown-header-playlistlocation) is set to 'before' or 'after', is an <ul> tag. So, if you'd like it to not be a list you should set [playlist.location](#markdown-header-playlistlocation) to 'external' and create your own 'cyt-playlist' element without the <ul> tag; additionally setting your own playlist.itemGenerate HTML string without an <li> tag.

[Table of Contents](#markdown-header-table-of-contents)

##Display Options
The following options are for the player's displays. Many of the options are only used to control the default YouTube displays.

###display.iframeHeight
**iframeHeight** (value: number) (default: 360)

Initial height, in pixels, of the generated <iframe>. This may be overridden in CSS.

[Table of Contents](#markdown-header-table-of-contents)

###display.iframeWidth
**iframeWidth** (value: number) (default: 640)

Initial height, in pixels, of the generated <iframe>. This may be overridden in CSS.

[Table of Contents](#markdown-header-table-of-contents)

###display.bareVideo
**bareVideo** (value: boolean) (default: false)

Whether or not to quickly strip the default displays of the YouTube video. Setting this to true is equivalent of doing to following display options:

* [display.hideControls](#markdown-header-displayhidecontrols): true
* [display.showInfo](#markdown-header-displayshowinfo): false
* [display.videoAnnotations](#markdown-header-displayvideoannotations): false
* [display.relatedVideos](#markdown-header-displayrelatedvideos): false

**NOTE:** At true, it additionally renders the following options redundant:

* [display.fullscreenButton](#markdown-header-displayfullscreenbutton)
* [display.color](#markdown-header-displaycolor)
* [display.modestBranding](#markdown-header-displaymodestbranding)
* [display.interfaceLanguage](#markdown-header-displayinterfacelanguage)

[Table of Contents](#markdown-header-table-of-contents)

###display.hideControls
**hideControls** (value: boolean) (default: false)

Wwhether or not to show YouTube's default player controls. It is not necessary to set this to true if [display.barevideo](#markdown-header-displaybarevideo) is set to true. 

**NOTE:** Setting this to true will render the following options redundant:

* [display.fullscreenButton](#markdown-header-displayfullscreenbutton)
* [display.color](#markdown-header-displaycolor)
* [display.modestBranding](#markdown-header-displaymodestbranding)

[Table of Contents](#markdown-header-table-of-contents)

###display.showInfo
**showInfo** (value: boolean) (default: true)

Whether or not information about the video (e.g. video title) will be shown. It is not necessary to set this option to false if [display.bareVideo](#markdown-header-displaybarevideo) is set to true.

[Table of Contents](#markdown-header-table-of-contents)

###display.videoAnnotations
**videoAnnotations** (value: boolean) (default: true)

Whether or not any YouTube annotations given to the video will appear. It is not necessary to set this option to false if [display.bareVideo](#markdown-header-displaybarevideo) is set to true.

[Table of Contents](#markdown-header-table-of-contents)

###display.relatedVideos
**relatedVideos** (value: boolean) (default: true)

Whether or not to show YouTube's suggested related videos when the video ends or is paused. It is not necessary to set this option to false if [display.bareVideo](#markdown-header-displaybarevideo) is set to true.

[Table of Contents](#markdown-header-table-of-contents)

###display.fullscreenButton
**fullscreenButton** (value: boolean) (default: true)

Whether or not to have the option to show YouTubes fullsreen button in it's default controls bar. It is not necessary to set this option to false if [display.bareVideo](#markdown-header-displaybarevideo) or [display.hideControls](#markdown-header-displayhidecontrols) is set to true.


[Table of Contents](#markdown-header-table-of-contents)

###display.color
**color** (value: string) (default: 'red')

* 'white' - [display.modestBranding](#markdown-header-displaymodestbranding) will automatically be set to false, and cannot be changed to true. Changes the color of the default YouTube progress bar to white. 
* 'red' (default) - Keeps default colors of the default YouTube interface and controls.

To fully customize the progress bar, and other controls, see the [Controls Options](#markdown-header-controls-options). It is not necessary to set this option if [display.bareVideo](#markdown-header-displaybarevideo) or [display.hideControls](#markdown-header-displayhidecontrols) is set to true.

[Table of Contents](#markdown-header-table-of-contents)

###display.modestBranding
**modestBranding** (value: boolean) (default: false)

Whether or not to have the YouTube button appear in the YouTube default player's control bar. Even if true it will still show if [display.color](#markdown-header-displaycolor) is set to 'white'. It is not necessary to set this option to true if [display.bareVideo](#markdown-header-displaybarevideo) or [display.hideControls](#markdown-header-displayhidecontrols) is set to true.

[Table of Contents](#markdown-header-table-of-contents)

###display.forceCC
**forceCC** (value: boolean) (default: false)

Whether or not to cause closed captions to be shown by default, even if the user has turned captions off. The default (false) behavior is based on user preference.

[Table of Contents](#markdown-header-table-of-contents)

###display.interfaceLanguage
**interfaceLanguage** (value: 'string' || false (boolean)) (default: false)

Can be set an ISO 639-1 two-letter language code or a fully specified locale. For example, fr and fr-ca are both valid values. 

The interface language is used for tooltips in the player and also affects the default caption track. Note that YouTube might select a different caption track language for a particular user based on the user's individual language preferences and the availability of caption tracks.

[Table of Contents](#markdown-header-table-of-contents)

##Custom Start Button Options

###customStartButton.useDefault
**useDefault** (value: boolean) (default: true)

* *true* (default) - Show YouTube defalt red start button to begin video.
* *false* - a poster image will be placed over the video to hide YouTube's default red start button. And allow a custom start button to be placed over it.

[Table of Contents](#markdown-header-table-of-contents)

###customStartButton.type
**type** (value: string) (default: 'none') (requires: [customStartButton.useDefault](#markdown-header-customstartbuttonusedefault): false)

What type of button is appended to the poster. See the following value options:

* 'none': (default) Do not append any kind of button.
* 'src': create an <img> tag. You would then need to set a value to [customStartButton.src](#markdown-header-customstartbuttonsrc) with the URI to the <img> src attribute. It will be appended inside a <button> tag and onto the poster.
* 'html': allow custom HTML to be placed in as the button, this is particularly useful when you want to use an external icon library that uses classes. Set [customStartButton.html](#markdown-header-customstartbuttonhtml) to an HTML string. It will already be placed inside a <button> tag and onto the poster.

[Table of Contents](#markdown-header-table-of-contents)

###customStartButton.src
**src** (value: URI string) (default: empty string) (requires: [[customStartButton.useDefault](#markdown-header-customstartbuttonusedefault): false] && [[customStartButton.type](#markdown-header-customstartbuttontype): 'src'])

Set the URI here, for the button's <img> src attribute.

[Table of Contents](#markdown-header-table-of-contents)

###customStartButton.html
**html** (value: HTML string) (default: '') (requires: [[customStartButton.useDefault](#markdown-header-customstartbuttonusedefault): false] && [[customStartButton.type](#markdown-header-customstartbuttontype): 'html'])

The HTML representing the button that you would like to be appended to the poster. This HTML string will already be placed inside a <button> tag.

[Table of Contents](#markdown-header-table-of-contents)

###customStartButton.width
**width** (value: string) (default: '25%') (requires: [[customStartButton.useDefault](#markdown-header-customstartbuttonusedefault): false] && [[customStartButton.type](#markdown-header-customstartbuttontype): 'html' | 'string'])

Whether [customStartButton.type](#markdown-header-customstartbuttontype) is set to 'html' or 'string', the button information is within a <button> tag. This width option sets the width of that <button>. The string must be in some CSS value that would work in the CSS width property.

[Table of Contents](#markdown-header-table-of-contents)

###customStartButton.height
**height** (value: string) (default: '25%') (requires: [[customStartButton.useDefault](#markdown-header-customstartbuttonusedefault): false] && [[customStartButton.type](#markdown-header-customstartbuttontype): 'html' | 'string'])

Whether [customStartButton.type](#markdown-header-customstartbuttontype) is set to 'html' or 'string', the button information is within a <button> tag. This height option sets the height of that <button>. The string must be in some CSS value that would work in the CSS height property.

[Table of Contents](#markdown-header-table-of-contents)

###customStartButton.position
**position** (value: string) (default: 'center') (requires: [[customStartButton.useDefault](#markdown-header-customstartbuttonusedefault): false] && [[customStartButton.type](#markdown-header-customstartbuttontype): 'html' | 'string'])

Determine the position that the button will appear over the poster. If the value is assigned to anything other than below, then no positioning would be applied and you would be free to do it in CSS. The possible values are below:

* 'left-top'
* 'left-center'
* 'left-bottom'
* 'center-top'
* 'center' (default)
* 'center-bottom'
* 'right-top'
* 'right-center'
* 'right-bottom'

[Table of Contents](#markdown-header-table-of-contents)

##Controls Options
The following option would configure various aspects of your controls, mainly custom controls. You would need to write your own HTML for *CutomYTPlayer* to hook into, you can learn more about this is under [Control Classes](#markdown-header-control-classes), [controls.location](#markdown-header-controlslocation), and [controls.html](#markdown-header-controlshtml)

###controls.custom
**custom** (value: boolean) (default: false)

Whether or not to hide the default YouTube controls, and allow you to add and enable your own custom controls. To disable/hide YouTube's default controls see [display.bareVideo](#markdown-header-displaybarevideo) or [display.hideControls](#markdown-header-displayhidecontrols). Both custom and default controls are possible to be present at the same time.

[Table of Contents](#markdown-header-table-of-contents)

###controls.allowKeyboardControls
**allowKeyboardControls** (value: boolean) (default: true)

Whether or not keyboard keys affect the player's controls.

[Table of Contents](#markdown-header-table-of-contents)

###controls.location
**location** (value: string) (default: 'external') (requires: [controls.custom](#markdown-header-controlscustom): true)

Location of your custom controls. The following are options for the value:

* 'external': HTML for the controls will not be generated but rather are only located outside of the player itself.
* 'before': this will require [controls.html](#markdown-header-controlshtml) to be set to an HTML string. Then, that HTML string will be placed within the player's outermost generated container but before the player itself. Any external controls will still also work.
* 'after': this will require [controls.html](#markdown-header-controlshtml) to be set to an HTML string. Then, that HTML string will be placed within the player's outermost generated container but after the player itself. Any external controls will still also work.

[Table of Contents](#markdown-header-table-of-contents)

###controls.html
**html** (value: HTML string) (default: empty string) (requires: [[controls.custom](#markdown-header-controlscustom): true] && [[custom.location](#markdown-header-customlocation)]: 'before' | 'after')

An HTML string, that houses all of the custom controls for the player. HTML classes is what determines functionality. The player will also work with custom controls outside of the player, when using the same classes. See the [Control Classes](#markdown-header-control-classes), for more information on how to make the custom HTML and the available classes.

[Table of Contents](#markdown-header-table-of-contents)

###controls.progressAxis
**progressAxis** (value: string) (default: 'horizontal') (requires: [controls.custom](#markdown-header-controlscustom): true)

Determines the direction in which the progress bar will progress. It is used in conjunction the [.cyt-progress-bar].(#markdown-header-cyt-progress-bar) class. Available options are:

* 'horizontal' (default)
* 'vertical'

[Table of Contents](#markdown-header-table-of-contents)

###controls.volumeStart
**volumeStart** (value: integer) (default: 50)

Determines the starting value (0 to 100) of player's volume. 

[Table of Contents](#markdown-header-table-of-contents)

###controls.volumeAxis
**volumeAxis** (value: string) (default: 'horizontal') (requires: [controls.custom](#markdown-header-controlscustom): true)

Determines the direction in which the volume bar progresses. It is used in conjunction with the ['.cyt-vol-bar'](#markdown-header-cyt-colume-bar) class. Available options are:

* 'horizontal' (default)
* 'vertical'

[Table of Contents](#markdown-header-table-of-contents)

##Control Classes
If [controls.custom](#markdown-header-controlscustom) is set to true then you may write custom HTML elements to control the player. The classes, and occasional data attributes, listed below are all you need to add to these HTML elements to control the player. Any external controls need to be placed in a containing element that has two classes. The first, is a class with the same name as your player's HTML id, with '-controls' appended to it. The second is simply 'cyt-controls'. You may have as many of these groups of controls as you want on an HTML page. Any internally generated controls (i.e. [controls.location](#markdown-header-controlslocation): 'before' or 'after') does not need these containing classes as it is already generated.

###.cyt-back-x
**Requires data-seconds attribute (integer)**

Moves the player's playtime back the amount of seconds specified in the data-seconds attribute. 

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-buffer-amount
Displays the amount the video has buffered in percentage, by updating the element's text.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-buffer-bar
Changes width of element per the percentage of the video that has been buffered. Usually well suited to be a child of the [.cyt-progress-container](#markdown-header-cyt-progress-container) element.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-current-time-amount
Displays the current playtime of the current video in the player, by updating the element's text. Updates as the video is played or scrubbed.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-duration-amount
Displays the duration time of the current video in the player, by updating the element's text.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-forward-x
**Requires data-seconds attribute (integer)**

Moves the player's playtime forward the amount of seconds specified in the data-seconds attribute. 

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-mute
Mutes the player.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-mute-toggle
Toggles between muting and unmuting the player.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-next
Queue next video in playlist.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-pause
Pause the current video.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-play
Play the current video.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-playback-rate
**Requires a data-rate attribute**
Sets the playback rate of the video per the amount specified in the data rate attribute. The possible rates are:

* 0.25
* 0.5
* 1
* 1.5
* 2

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-playback-rate-amount
Displays the current playback rate, by updating the target element's text. Will update if the playback rate changes.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-play-pause
Toggle playing or pausing the current video. If the video is neither currently playing or paused, the video will play.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-prev
Queue previous video in the playlist.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-progress-container and .cyt-progress-bar
**.cyt-progress-container requires child element with a .cyt-progress-bar class**

Creates a scrubbable progress bar. The axis of the bar depends on how [controls.progressAxis](#markdown-header-controlsprogressaxis) is set. You may need to style heights and widths to these elements with CSS.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-unmute
Unmutes the player

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-vol-container and .cyt-vol-bar
**.cyt-vol-container requires child element with a .cyt-vol-bar class**

Creates scrubbable volume. The axis of the bar depends on how [controls.volumeAxis](#markdown-header-controlsvolumeaxis) is set. You may need to style heights and widths to these elements with CSS.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-vol-down-x
**Requires data-percent attribute (Integer 1-100)**

Turn volume down per amount provided in data-percent.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-vol-amount
Displays volume amount (1-100), by changing target element's text.

[Table of Contents](#markdown-header-table-of-contents)

###.cyt-vol-up-x
**Requires data-percent attribute (Integer 1-100)**

Turn volume up per amount provided in data-percent.

[Table of Contents](#markdown-header-table-of-contents)